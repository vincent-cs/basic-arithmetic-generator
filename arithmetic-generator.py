#!/usr/bin/env python
# the shebang may or not be necessary, but it can solve problems.
# essentially it ensures that when running this program in shell it will use your current (active) python environment.

# intro
# This requires python 3.6 or newer
# If you would like to change it to older version of python 3, such as 3.5 or older
# You would have to not use the secrets module and rather use the random module.
# we use the secrets module for better theoretical randomization than the random module
# whether or not the secrets module is actually better is debatable, however it does require python 3.6.
# keep in mind that no number generated by a computer is truly random, there is always some form of repetition.
# another change for python 2.* is you would have to change the input() to raw_input()
# some of the behaviour regarding integers, floats, and arithmetic operators may also be different.

# dependencies:
# python 3.6

# we use this for strong random numbers, requires python 3.6
import secrets

# we use this a few times to delay print statements, only if the user asks for "fun" messages.
# if the user says no to fun messages, this module is not used whatsoever
import time


# begin functions

# begin fun messages functions

# ask user if they would like to have 'fun' messages as part of their experience
def get_pref_fun_messages(pref_fun_messages):
    pref_fun_messages = input('Please enter whether you would like to enable \"fun\" messages as you play (yes/no): ')
    if pref_fun_messages == 'yes' or pref_fun_messages == 'y' or pref_fun_messages == 'Yes' \
            or pref_fun_messages == 'Y' or pref_fun_messages == 'ye' or pref_fun_messages == 't' \
            or pref_fun_messages == 'true' or pref_fun_messages == 'True':
        pref_fun_messages = True
        print('Fun messages are now enabled.')
        # enable the fun messages
        return True
    else:
        pref_fun_messages = False
        print('Fun messages are now disabled.')
        return False


# if user has agreed to fun messages, we will give them to them randomly.
# we will have several functions to call for. Some of these will give a motivational message, some of these will give
# not so motivation messages. The point is is that it should be random and unpredictable.
# if they do not enable this


# this is a function to give messages when the program is starting.
def generate_beginning_fun_message(pref_fun_messages):
    if pref_fun_messages is True:
        number = secrets.randbelow(20)
        if number == 1:
            print('Always on the move.')
        elif number == 2:
            print('This is where the fun begins!')
        elif number == 3:
            print('Hold on, this whole operation was your idea.')
        elif number == 4:
            print('Oh, this is going to be easy.')
        elif number == 5:
            print('I think you\'ll be needing this! *hands you pencil*.')
        print('')
        time.sleep(0.5)
        return True
    else:
        return False


# this function gives a message after the user makes their first choice/input
def generate_first_input_fun_message(pref_fun_messages):
    if pref_fun_messages is True:
        print('You have chosen ' + chosen_operation + ', great choice!')
        time.sleep(0.5)
        print('')
        return True
    else:
        return False


# this function gives a message after the user makes their second choice/input
def generate_second_input_fun_message(pref_fun_messages):
    if pref_fun_messages is True:
        print('You are doing ' + str(chosen_questions) + ' ' + chosen_operation + ' questions.')
        time.sleep(0.5)
        return True
    else:
        return False


# this function gives a motivational message after one has completed a question
def generate_encouraging_fun_message(pref_fun_messages):
    if pref_fun_messages is True:
        # if you would like to make these messages more common or rarer, adjust the integer below in secrets.randbelow()
        number = secrets.randbelow(40)
        if number == 1:
            print('Wowzers')
        elif number == 2:
            print('Huh, I\'m a computer and I couldn\'t even guess.')
        elif number == 3:
            print('I would totally order you a pizza if I had money')
        elif number == 4:
            print('good job!')
        else:
            print('Correct!')
    # we will print "Correct!" a "fun" message has not been chosen, or only "Correct!" if the user has disabled
    # "fun" messages.
    else:
        print('Correct!')
    return pref_fun_messages


# this function gives a discouraging (mean) message after one has failed a question
def generate_discouraging_fun_message(pref_fun_messages):
    if pref_fun_messages is True:
        # if you would like to make these messages more common or rarer, adjust the integer below in secrets.randbelow()
        # in secrets.randbelow()
        number = secrets.randbelow(40)
        if number == 1:
            print('It can\'t be that difficult')
        elif number == 2:
            print('Just do math')
        elif number == 3:
            print('Just use the formula')
        elif number == 4:
            print('Twice the pride, double the fall')
        elif number == 5:
            print('Math is just applied intelligence')
        elif number == 6:
            print('I was hoping for the correct answer, why did you answer wrongly?')
        elif number == 7:
            print('Maybe next time you will be less wrong')
        else:
            print('Incorrect!')
        # we will print "Incorrect!" a "fun" message has not been chosen, or only "Incorrect!" 
        # if the user has disabled "fun" messages.
    else:
        print('Incorrect!')
    return pref_fun_messages


# end fun messages functions

# begin functions to run the arithmetic generator

# ask user what basic arithmetic operation they would like to practice
def get_desired_operation(desired_operation):
    while True:

        # get how many questions we want
        desired_operation = input('Enter which arithmetic operation you would like to practice: ')

        # take input and lower it to make it easier for our program to recognize.
        desired_operation = str.lower(desired_operation)

        # do this in case the user enters a derivative of a arithmetic
        # operator the program will still recognize it.
        # we will check for other common variants of the 4 basic arithmetic operations
        if desired_operation == 'add':
            desired_operation = 'addition'
        elif desired_operation == 'a':
            desired_operation = 'addition'
        elif 'add' in desired_operation:
            desired_operation = 'addition'

        elif desired_operation == 'subtract':
            desired_operation = 'subtraction'
        elif desired_operation == 's':
            desired_operation = 'subtraction'
        elif 'min' in desired_operation:
            desired_operation = 'subtraction'
        elif 'sub' in desired_operation:
            desired_operation = 'subtraction'

        elif desired_operation == 'multiply':
            desired_operation = 'multiplication'
        elif desired_operation == 'm':
            desired_operation = 'multiplication'
        elif 'tim' in desired_operation:
            desired_operation = 'multiplication'

        elif desired_operation == 'divide':
            desired_operation = 'division'
        elif desired_operation == 'd':
            desired_operation = 'division'
        elif 'div' in desired_operation:
            desired_operation = 'division'

        # if the user has given us a valid input we can return that
        # otherwise try try again
        if desired_operation == 'addition' or desired_operation == 'subtraction' or desired_operation == \
                'multiplication' or desired_operation == 'division':
            return desired_operation
        else:
            print('You have not asked for a compatible arithmetic operator.')
            print('These are currently limited to the 4 basic arithmetic operators (spelled correctly).')
            print('Try entering \"a\", \"s\", \"m\" or \"d\" for quick selection.')
            print('')


# ask the user how many questions they would like to do.
def get_desired_questions(desired_questions):
    # ask user how many questions they would like to do
    while True:
        desired_questions = input('Enter how many questions you would like to do: ')
        # will give ValueError if the desired_questions is not an integer.
        # normally program would just exit with exit code 1 (crash) and then the user would have to restart
        # rather we will print a nice error for the user so they can fix their input, and continue where they were.

        try:
            desired_questions = int(desired_questions)
            below_zero_message = True
        except ValueError:
            print(
                'Please enter a natural number. \"{0}\" is not recognized as a valid amount of questions that you '
                'can practice.'.format(str(desired_questions)))
            print('')
            below_zero_message = False

        try:
            if desired_questions <= 0 and below_zero_message is True:
                print('You can\'t do ' + str(desired_questions) + ' questions! Don\'t you want to practice math?')
                print('')
            else:
                return desired_questions
        except TypeError:
            pass


# ask user for range
def get_desired_range(desired_range):

    # we use this function to find both ranges, so we need to make sure the function knows what range we are finding
    # we get this from the get_min_range & get_max_range functions (shown below)
    which_range_to_find = desired_range
    desired_range_is_int = False

    while True:
        desired_range = input(
            'Enter the ' + str(which_range_to_find) + ' value we should use in these ' + chosen_operation
            + ' questions: ')

        try:
            desired_range = int(desired_range)
            desired_range_is_int = True
            if desired_range <= 0:
                pass
            # will give ValueError if the desired_range is not an integer and cannot be converted from word format.
            # normally program would just exit with code 1 (essentially crash) and then the user would have to restart
            # rather we will print a nice error for the user so they can fix their input.
            # if you haven't notice by new, we do this a lot
        except ValueError or TypeError:
            if chosen_operation == 'addition' or chosen_operation == 'subtraction':
                print('Please enter an integer from 1 to 100. \"' + str(
                    desired_range) + '\" is not recognized as a valid ' + which_range_to_find + ' range for questions.')
                print('')
                desired_range_is_int = False
            else:
                print('')
                print('Please enter an integer from 1 to 15. \"' + str(
                    desired_range) + '\" is not recognized as a valid ' + which_range_to_find + ' range for questions.')
                print('')
                desired_range_is_int = False

        # now that we know the number is an integer, we can than check validity based on the requirements.
        # we have a greater range for addition and subtraction questions
        if desired_range_is_int is True:
            if chosen_operation is 'addition' or chosen_operation is 'subtraction':
                if 1 <= desired_range <= 100:
                    return desired_range
                elif desired_range > 100:
                    print('Please enter an integer from 1 to 100 only. You chose ' + str(
                        desired_range) + ', which is past 100.')
                    print('')
                elif desired_range < 1:
                    print('Please enter an integer from 1 to 100 only. You chose ' + str(
                        desired_range) + ', which is below 1.')
                    print('')
                else:
                    print('Please enter an integer from 1 to 100 only. You chose ' + str(
                        desired_range) + ', which is not recognized.')
                    print('')

            # we have a lesser range for multiplication and division questions
            elif chosen_operation == 'multiplication' or chosen_operation == 'division':
                if 1 <= desired_range <= 15:
                    return desired_range
                elif desired_range > 15 and desired_range_is_int is True:
                    print('Please enter an integer from 1 to 15 only. You chose ' + str(desired_range)
                          + ', which is past 15.')
                    print('')
                elif desired_range < 1 and desired_range_is_int is True:
                    print('Please enter an integer from 1 to 15 only. You chose ' + str(desired_range)
                          + ', which is below 1.')
                    print('')
                elif desired_range_is_int is True:
                    print('Please enter an integer from 1 to 15 only. You chose ' + str(desired_range)
                          + ', which is not recognized.')
                    print('')


# get the minimum range
def get_min_range(desired_min_range):
    desired_range = 'minimum'
    desired_min_range = get_desired_range(desired_range)
    print('')
    return desired_min_range


# get the maximum range
def get_max_range(desired_max_range):
    desired_range = 'maximum'
    while True:
        desired_max_range = get_desired_range(desired_range)
        # we should check if the desired max range is lower than the minimum range
        if chosen_min_range > desired_max_range:
            print('Your maximum range (' + str(desired_max_range) + ') is lower than your minimum range (' + str(
                chosen_min_range) + '). Please make the maximum greater than or equal to the minimum.')
            print('')
        else:
            return desired_max_range


# end range functions

# begin calculation functions

# find symbol of operation, used in generating the questions
def determine_operation_symbol(chosen_operation):
    if chosen_operation == 'addition':
        operation_symbol = '+'
    elif chosen_operation == 'subtraction':
        operation_symbol = '-'
    elif chosen_operation == 'multiplication':
        operation_symbol = '*'
    elif chosen_operation == 'division':
        operation_symbol = '/'
    return operation_symbol


# find word describing the operation, used in generating the questions
def determine_operation_word(chosen_operation):
    if chosen_operation == 'addition':
        operation_word = 'sum'
    elif chosen_operation == 'subtraction':
        operation_word = 'difference'
    elif chosen_operation == 'multiplication':
        operation_word = 'product'
    elif chosen_operation == 'division':
        operation_word = 'quotient'
    return operation_word


# question generator
def generate_question(end_program, correct_answers, incorrect_answers):

    # get the 2 numbers from the ranges provided by the user
    # at the beginning there is an explanation for the use of secrets
    a = secrets.choice(range(chosen_min_range, chosen_max_range))
    b = secrets.choice(range(chosen_min_range, chosen_max_range))

    end_program = False
    accumulated_correct_answers = 0
    accumulated_incorrect_answers = 0

    while True:
        # find the correct answer for verification later on
        if chosen_operation == 'addition':
            correct_answer = int(a + b)
        elif chosen_operation == 'subtraction':
            correct_answer = int(a - b)
        elif chosen_operation == 'multiplication':
            correct_answer = int(a * b)
        elif chosen_operation == 'division':
            correct_answer = int((a * b) / b)
            # the division logic is different so we do not get decimal places in our correct answers.
            # that makes it far to difficult to solve in your head.

        # ask the question, ask it differently if it is division.
        if chosen_operation != 'division':
            user_answer = input(
                'What is the {0} of {1} {2} {3}? '.format(operation_word, str(a), str(operation_symbol), str(b)))
        else:
            user_answer = input('Enter the ' + operation_word + ' of ' + str(a * b) + ' ' + str(operation_symbol) +
                                ' ' + str(b) + ': ')

        # check if user has told us to end the program, if so end everything immediately.
        if user_answer == 'q' or user_answer == 'quit':
            print('')
            end_program = True
            return end_program, accumulated_correct_answers, accumulated_incorrect_answers

        # see if the user as entered a proper answer
        # if the answer is an integer that is good and it will pass this test
        # otherwise we are going to tell the user
        try:
            user_answer = int(user_answer)
            user_answer_is_valid = True
            # if this can't be done we will give the user an error.
            # we will only every accept integers as an answer, so we can exclude floats.
        except ValueError or TypeError:
            print('Please enter an integer as your answer.')
            print('')
            # we will set this to false as we do not want to give a "Incorrect!" message if the user has just submitted
            # bad input.
            # if the user enters invalid input it will not be counted as a bad guess later on in the elif statement.
            user_answer_is_valid = False

        # now see if the user has answered the question correctly
        if user_answer is correct_answer:

            # give an encouraging message because they got the answer right
            generate_encouraging_fun_message(pref_fun_messages)
            print('')

            # increase the counter, which we use later to summarize to the user how well they did.
            accumulated_correct_answers = accumulated_correct_answers + 1
            return end_program, accumulated_correct_answers, accumulated_incorrect_answers

        # we will only give this if the user answers incorrectly (only if the answer passes the validity check)
        elif user_answer_is_valid is True:

            # give an discouraging message because they got the answer wrong
            generate_discouraging_fun_message(pref_fun_messages)

            # increase the counter, which we use later to summarize to the user how well they did.
            # we accumulate a certain amount of answers/guesses each time the user does one of these questions,
            # when one question is complete we add this accumulation to the total counter
            accumulated_incorrect_answers = accumulated_incorrect_answers + 1
            print('')


# end functions
########################################################################################################################
# begin program
# beginning sequence, inputs and messages

print('Basic Arithmetic Generator  Copyright (C) 2019  Vincent Chernin')
print('This program comes with ABSOLUTELY NO WARRANTY.')
print('')

print('Welcome to Vincent\'s arithmetic practice game.')
print('')

# ask if user wants fun messages, they may want to skip these as they might quickly become annoying.
# we use this a lot in the future.
pref_fun_messages = ''
pref_fun_messages = get_pref_fun_messages(pref_fun_messages)
print('')

# find out what operation the user would like to practice
desired_operation = ''
chosen_operation = get_desired_operation(desired_operation)
print('')

# post first fun message if desired
generate_first_input_fun_message(pref_fun_messages)

# find out how many questions the user should be asked
desired_questions = ''
chosen_questions = int(get_desired_questions(desired_questions))
print('')

# post second fun message is desired
generate_second_input_fun_message(pref_fun_messages)

# post fun message if desired
generate_beginning_fun_message(pref_fun_messages)

# get ranges
chosen_min_range = get_min_range('')
chosen_max_range = get_max_range('')

# now that we have all the inputs, we can summarize them to the user.
print('')
print('Now you will do ' + str(chosen_questions) + ' ' + str(
    chosen_operation) + ' questions, with numbers ranging from ' + str(chosen_min_range)
      + ' to ' + str(chosen_max_range) + '.')

# we should note that division works differently in order to achieve whole numbers
# (one should not need a calculator for any of these questions)
if chosen_operation == 'division':
    print('In order to make these division questions doable without a calculator, we increase the numbers '
          'used in questions using the ranges provided.')
    print('We are still using the requested range, just using it differently.')
    print('')

# be clear if they can redo questions
print('You must answer a question correctly to move on to the next; there is no way to skip questions.')

# we should be clear about that they can quit the game at any time.
print('You can end the program at any time during question answering by entering \"q\".')
print('')

# we will these for generating questions
operation_symbol = determine_operation_symbol(chosen_operation)
operation_word = determine_operation_word(chosen_operation)

# use question generator as many times as we need
end_program = False

# we will start with this many correct answers/guesses by the user, and collect more for each time the question
# generator operates

# these pairs of variables serve different purposes, one counts the total count given to the user at the end,
# the other counts during each time the generator is run and then adds it count to the total
total_correct_answers = 0
total_incorrect_answers = 0

accumulated_correct_answers = 0
accumulated_incorrect_answers = 0

# one small issue with the secrets module is that the range() function does not include the max range itself.
# for example, if the user chooses min = 3 and max = 5, the program will only use the numbers 3 to 4.
# Thus this line is necessary to fix this inconsistency.

# however, we must do this line after printing the max range, as then it would be one higher than it should be.
# we could also create 2 separate variables, but that seems silly.

chosen_max_range = chosen_max_range + 1

# ask questions, and counts the total correct and incorrect answers for use later
# we run this loop until the user presses 'q' or complete all the required questions.
while True:
    # the question generator gives 3 outputs: run program, correct answers and incorrect answers.
    question_generator_output = generate_question(end_program, accumulated_correct_answers,
                                                  accumulated_incorrect_answers)

    # see what the question generator outputs, if it is true we must end the program
    end_program = question_generator_output[0]

    # we want to count the total accumulated correct and incorrect answers
    total_correct_answers = total_correct_answers + question_generator_output[1]
    total_incorrect_answers = total_incorrect_answers + question_generator_output[2]

    # we check if the user has either asked to end the program or finished all the questions.
    if end_program is True or total_correct_answers == chosen_questions:
        break

# end the game, and gives the user a recap over what they did
# this uses numbers previously collected by the question generator

print('You have completed ' + str(total_correct_answers) + ' questions in ' +
      str(total_correct_answers + total_incorrect_answers) + ' answers.')

try:
    print('This gives you an accuracy rate of ' + str(
        round(((total_correct_answers / (total_correct_answers + total_incorrect_answers)) * 100), 2)) + '%.')
except ZeroDivisionError:
    print('This gives you an accuracy rate of 0%.')

# prints how many correct answers the user has done.
if total_correct_answers == chosen_questions:
    # as mentioned in the vincent-errors document, here we alter behavior depending if the user is doing one questions.
    # there are likely more efficient ways of doing this, most likely using a function, but still it is not useful.
    if chosen_questions == 1:
        print('You have answered the ' + str(chosen_questions) + ' question you asked to do.')
    else:
        print('You have answered the ' + str(chosen_questions) + ' questions you asked to do.')
else:
    print(
        'You have done ' + str(total_correct_answers) + ' out of the ' + str(chosen_questions) + ' questions you asked'
                                                                                                 ' to do.')
